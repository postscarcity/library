# Contributing

> This page contains information on how to contribute to an **existing** library page.
If you want to add a new page or rename an existing page, please check [here](https://gitlab.com/postscarcity/map/-/blob/main/CONTRIBUTING.md).

**Summary**

Be as unambiguous and impartial as possible, reference any figure you provide and follow the style of the [template](https://gitlab.com/postscarcity/library/-/blob/main/TEMPLATE.md). 

A full example can be found [here](https://gitlab.com/postscarcity/library/-/blob/main/food_robots.md).

**Table of Contents**

[TOC]

# Guidelines

Thank you for taking the time to contribute and to read our guidelines.

We aim for the contributions to the library to be: 

**Factual**

Every figure needs to be referenced with accessible and reliable sources. 

**Transparent**

Disclose conflicts of interest or other biases you may be aware of. 

**Neutral**

Report figures and material with the minimum amount of editorial editing possible. 

# Structure

Each page follows a similar structure, as outlined in the [template](https://gitlab.com/postscarcity/library/-/blob/main/TEMPLATE.md), comprising three sections (resources, sources and directory) with an additional state of the art section for milestone pages. 

## Milestone

One sentence outlining the quantifiable milestone that needs to be reached. 

Highlight the objective in **bold**.

> **Example**: Food Robot with average time between minor human interventions **< 3 hours**

## Description

[_Optional_] More detailed description of the condition.

Ambiguous terms need to be clarified here to avoid cluttering the main milestone description. 

The goal is to ensure that different people reading the milestone description would be able to evaluate its completion as unequivocably as possible. 

Clarification can be qualitative (**Example 1**) or quantitative (**Example 2**) in nature.

> **Example 1**: Human intervention is needed if the robot enters an error state. 

> **Example 2**: A human intervention is minor if the robot system can be restarted in < 60 minutes, without need for repairs.

## State of the Art

This section presents the current state of the art, the year when it was first achieved and the primary source. 

When two or more state of the arts exist for the same milestone, concurrent claims can be included, with the oldest being the most prominent and the others listed below.  

To fairly assess the reliability of the information provided, we use three levels. 

### **Claimed**

Performance data has only been published by the claiming party.

> **Examples**: preprint papers, company press releases.

### **Verified**

Performance data has been scrutinised / verified by at least one external third party not affiliated with the original claiming party.

> **Examples**: published paper, product beta testing.

### **Public**

Product / service is available and can be utilised as intented.

> **Examples**: product available on the market. 

## Resources

Material (e.g. papers) published as a one-off or only edited in limited form.

## Sources

Place of origin of the resources (e.g. journals) or aggregators (e.g. blogs).

## Directory

Individuals, organisations (group of individuals) or associations (group of organisations) involved in the field / achievement of the current state of art or actively working on improving on it.

# Formatting

We use [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html), which allows for additional functionalities compared to standard Markdown, including [colours](https://docs.gitlab.com/ee/user/markdown.html#colors), [diagrams](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts), [emoji](https://docs.gitlab.com/ee/user/markdown.html#emojis), [LaTeX](https://docs.gitlab.com/ee/user/markdown.html#math) and [Wiki formatting](https://docs.gitlab.com/ee/user/markdown.html#wiki-specific-markdown).  

## Conventions

General formatting conventions include:

* **Original titles / names** are strongly preferred

* **Expand acronyms** at least the first time you use them, especially for non-mainstream terms

* **Chronological order** for resources

* **Alphabetical order** for sources and directory with names of individuals ordered by surname
