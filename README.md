# Library

[TOC]

**!! WIP !!**: Many entries are still missing, you may have been redirected here from a broken link.

# Purpose

This repository contains supporting evidence for the technological state of the art and learning material to make existing knowledge more accessible.

This library serves as a collection of references for the technology tree of the [Post-Scarcity Map](https://gitlab.com/postscarcity/map) project. 

# Material

To each node of the tree corresponds a page with the following sections:

* **Resources**: material (e.g. papers) published as a one-off or only edited in limited form


* **Sources** : place of origin pf the resources (e.g. journals) or aggregators (e.g. blogs)


* **Directory**: list of individuals, organisations (group of individuals) or associations (group of organisations) involved in the field / achievement of the current state of art or actively working on improving on it

# Milestones

Milestone nodes also contain a State of the Art section, where the current performance level and their supporting evidence are listed. 
To account for the variability in source reliability, three levels are introduced:

1. **Claimed**: performance data has only been published by the claiming party (e.g. preprint paper).

2. **Verified**: performance data has been scrutinised / verified by at least one external third party with minimal conflict of interest (e.g. published paper) 

3. **Public**: product / service is available 

# Contributing

This is an open and collaborative project. 

You can find more information on how to contribute and our general guidelines [here](https://gitlab.com/postscarcity/library/-/blob/main/CONTRIBUTING.md).
