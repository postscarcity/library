# Milestone

Milestone condition {operator} **X {units}**

> **Example**: Food Robot with average time between minor human interventions **< 3 hours**

[TOC]

## Description
[_Optional_] More detailed description of the condition, including quantification of terms whenever necessary.  

> **Example 1**: Human intervention is needed if the robot enters an error state. 

> **Example 2**: A human intervention is minor if the robot system can be restarted in < 60 minutes, without need for repairs.

# State of the Art

### Claimed: 2 hours 
> 2020 [Source](https://www.wikipedia.org/)

Description of the claim.

Concurrent claims can be submitted as well.

> 2022 [Alternative #1](https://www.wikipedia.org/)

> 2021 [Alternative #2](https://www.wikipedia.org/)

### Verified: 1 hour

> 2022 [Source](https://www.wikipedia.org/)

Description of the verified claim.

### Public: 30 minutes

> 2022 [Source](https://www.wikipedia.org/)

Description of the publicly available state of the art product / service.

# Resources

### Papers
 
- 2020 [Paper # 1 title](https://arxiv.org/)
- 2019 [Paper # 2 title](https://arxiv.org/)
- 2018 [Literature review](https://arxiv.org/)

### Patents

- 2017 [Patent](https://patentscope.wipo.int/search/en/search.jsf)

### Books

- 2017 [Book](https://wikipedia.org/)

### Courses

- 2020 [Course](https://ocw.mit.edu/)

### Reports

- 2017 [Report](https://wikipedia.org/)

### Posts

- 2018 [Blog post](https://wikipedia.org/)

# Sources

### Journals

- [Journal](https://wikipedia.org/)

### Wiki

- [Page](https://wikipedia.org/)

### News

- [Website](https://wikipedia.org/)

### Blogs

- [Blog](https://wikipedia.org/)

### Channels

- [Channel](https://wikipedia.org/)

### Podcasts

- [Podcast](https://wikipedia.org/)

# Directory

### Academia

- [Individual](https://wikipedia.org/)
- [Lab](https://wikipedia.org/)
- [Initiative](https://wikipedia.org/)

### Industry

- [Company](https://wikipedia.org/)
- [Team](https://wikipedia.org/)

### Government

- [Research centre](https://wikipedia.org/)

### NGOs

- [Institute](https://wikipedia.org/)
- [Association](https://wikipedia.org/)

### Communities

- [Community](https://wikipedia.org/)

### Conferences

- [Conference](https://wikipedia.org/)
