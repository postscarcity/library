# Milestone

Autonomous Truck able to travel on US-style highways for 500 km without human intervention

[TOC]

# State of the Art

### Claimed: 129 km
> 2021 [Driverless Truck Succesfully Travels More Than 129 km on Open Public Roads](https://www.autoevolution.com/news/driverless-truck-succesfully-travels-more-than-80-miles-on-open-public-roads-177803.html)

California-based TuSimple claims to be the world's first company to successfully operate a fully autonomous truck on open public roads. The journey was completed on December 22nd, and it involved a class 8 semi-truck that drove without a human on board for more than 80 miles (129 km) in real-life settings.


